# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/go-ozzo/ozzo-validation/v4 v4.3.0
## explicit; go 1.13
github.com/go-ozzo/ozzo-validation/v4
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
github.com/gorilla/mux
# github.com/lib/pq v1.10.7
## explicit; go 1.13
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/stretchr/testify v1.8.0
## explicit; go 1.13
# golang.org/x/crypto v0.1.0
## explicit; go 1.17
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
# gopkg.in/paytm/grace.v1 v1.0.0-20150417064046-edb50d497280
## explicit
gopkg.in/paytm/grace.v1
# gopkg.in/tylerb/graceful.v1 v1.2.15
## explicit
gopkg.in/tylerb/graceful.v1
