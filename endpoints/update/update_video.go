package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gorilla/mux"
)

type UpdateVideo struct {
	repo repository.Repository
}

func NewUpdateVideo(repo repository.Repository) (*UpdateVideo, error) {
	return &UpdateVideo{
		repo: repo,
	}, nil
}
func (create *UpdateVideo) UpdateVideo(w http.ResponseWriter, r *http.Request) {
	var param model.VideoDTO
	json.NewDecoder(r.Body).Decode(&param)
	err, id := validateUpdateVideo(mux.Vars(r))
	if err == nil {
		result, err := create.repo.UpdateVideoRepository(id, param)
		if err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			json.NewEncoder(w).Encode(model.NewError(http.StatusServiceUnavailable, err.Error()))
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(result)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id, comment is required"))
	}

}

func validateUpdateVideo(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}
}
