package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gorilla/mux"
)

type UpdateComment struct {
	repo repository.Repository
}

func NewUpdateComment(repo repository.Repository) (*UpdateComment, error) {
	return &UpdateComment{
		repo: repo,
	}, nil
}
func (create *UpdateComment) UpdateComments(w http.ResponseWriter, r *http.Request) {
	var param model.Comment
	json.NewDecoder(r.Body).Decode(&param)
	err, id := validateUpdateComment(mux.Vars(r))
	if err == nil {
		comments, err := create.repo.UpdateCommentInRepository(id, param)
		if err != nil {
			panic(err)
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(comments)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id, comment is required"))
	}
}

func validateUpdateComment(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}
}
