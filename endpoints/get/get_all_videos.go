package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"
)

type GetVideos struct {
	repo repository.Repository
}

func NewGetVideos(repo repository.Repository) (*GetVideos, error) {
	return &GetVideos{
		repo: repo,
	}, nil
}
func (getVideos *GetVideos) GetAllVideos(w http.ResponseWriter, r *http.Request) {
	video, err := getVideos.repo.GetVideosFromRepository()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusServiceUnavailable, "service unavailable"))
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(video)
}
