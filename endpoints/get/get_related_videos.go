package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"

	"github.com/gorilla/mux"
)

type RelatedVideos struct {
	repo repository.Repository
}

func NewGetRelatedVideos(repo repository.Repository) (*Video, error) {
	return &Video{
		repo: repo,
	}, nil
}

func (g *Video) GetRelatedVideos(w http.ResponseWriter, r *http.Request) {
	err, id := validateGetRelatedVideos(mux.Vars(r))
	user_id := r.URL.Query().Get("user_id")
	if user_id == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "UserId is required"))
	}
	if err == nil {
		video, err := g.repo.GetRelatedVideosRepository(id, user_id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			json.NewEncoder(w).Encode(model.NewError(http.StatusNotFound, "Video not found"))
		} else {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(video)
		}

	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id is required"))
	}
}

func validateGetRelatedVideos(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err != nil {
		return err, ""

	}
	return nil, data["id"]
}
