package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gorilla/mux"
)

type GetUserVideos struct {
	repo repository.Repository
}

func NewGetUserVideos(repo repository.Repository) (*GetUserVideos, error) {
	return &GetUserVideos{
		repo: repo,
	}, nil
}
func (getVideos *GetUserVideos) GetUserVideos(w http.ResponseWriter, r *http.Request) {
	err, id := validateGetUserVideos(mux.Vars(r))
	video, err := getVideos.repo.GetUserVideosFromRepository(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusServiceUnavailable, "service unavailable"))
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(video)
}

func validateGetUserVideos(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("user_id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["user_id"]
	} else {
		return err, ""
	}
}
