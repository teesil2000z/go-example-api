package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"

	"github.com/gorilla/mux"
)

type Video struct {
	repo repository.Repository
}

func NewGetVideo(repo repository.Repository) (*Video, error) {
	return &Video{
		repo: repo,
	}, nil
}

func (g *Video) GetVideo(w http.ResponseWriter, r *http.Request) {
	err, id := validateGetVideo(mux.Vars(r))
	if err == nil {
		video, err := g.repo.GetVideoFromRepository(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			json.NewEncoder(w).Encode(model.NewError(http.StatusNotFound, "Video not found"))
		} else {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(video)
		}

	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id is required"))
	}
}

func validateGetVideo(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}

}
