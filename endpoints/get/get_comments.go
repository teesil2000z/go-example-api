package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"

	validation "github.com/go-ozzo/ozzo-validation/v4"

	"net/http"

	"github.com/gorilla/mux"
)

type Comments struct {
	repo repository.Repository
}

func NewGetComments(repo repository.Repository) (*Comments, error) {
	return &Comments{
		repo: repo,
	}, nil
}

func (g *Comments) GetComments(w http.ResponseWriter, r *http.Request) {
	err, id := validateGetComments(mux.Vars(r))
	if err == nil {
		comments, err := g.repo.GetVideoCommentsFromRepository(id)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, err.Error()))
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(comments)
		return
	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id is required"))
		return
	}

}
func validateGetComments(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}
}
