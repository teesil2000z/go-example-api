package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gorilla/mux"
)

type DeleteVideo struct {
	repo repository.Repository
}

func NewDeleteVideo(repo repository.Repository) (*DeleteVideo, error) {
	return &DeleteVideo{
		repo: repo,
	}, nil
}
func (delete *DeleteVideo) DeleteVideo(w http.ResponseWriter, r *http.Request) {
	var param model.DeleteVideoParams
	json.NewDecoder(r.Body).Decode(&param)
	err, id := validateDeleteVideo(mux.Vars(r))
	if err == nil {
		comments, err := delete.repo.DeleteVideoInRepository(id, param)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id, comment is required"))
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(comments)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "Id, comment is required"))
	}
}

func validateDeleteVideo(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}
}
