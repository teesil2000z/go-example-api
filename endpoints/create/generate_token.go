package endpoints

import (
	"bloc_example/auth"
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"
)

type Token struct {
	repo repository.Repository
}

func NewToken(repo repository.Repository) (*Token, error) {
	return &Token{
		repo: repo,
	}, nil
}

func (generate *Token) GenerateToken(w http.ResponseWriter, r *http.Request) {
	var param model.Auth
	json.NewDecoder(r.Body).Decode(&param)
	user, err := generate.repo.CheckUserCredentialsDB(param.Email)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, err.Error()))
		return
	}

	credentialError := user.CheckPassword(param.Password)
	if credentialError != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, "invalid credentials"))
		return
	}

	tokenString, err := auth.GenerateJWT(param.Email, param.Password)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(model.NewError(http.StatusBadRequest, err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(model.NewToken(tokenString, user.Name, user.Id, user.Email))
}
