package endpoints

import (
	model "bloc_example/models"
	repository "bloc_example/repository"
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type CreateVideo struct {
	repo repository.Repository
}

func NewVideo(repo repository.Repository) (*CreateVideo, error) {
	return &CreateVideo{
		repo: repo,
	}, nil
}
func (create *CreateVideo) CreateVideo(w http.ResponseWriter, r *http.Request) {
	var param model.VideoDTO
	json.NewDecoder(r.Body).Decode(&param)
	result, err := create.repo.CreateVideoRepository(param)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(model.NewError(http.StatusServiceUnavailable, err.Error()))
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(result)

}

func validateCreateVideo(data map[string]string) (error, string) {
	err := validation.Validate(data,
		validation.Required,
		validation.Map(
			validation.Key("id", validation.Required),
		),
	)
	if err == nil {
		return nil, data["id"]
	} else {
		return err, ""
	}
}
