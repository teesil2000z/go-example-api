package router

import (
	create "bloc_example/endpoints/create"
	delete "bloc_example/endpoints/delete"
	get "bloc_example/endpoints/get"
	update "bloc_example/endpoints/update"
	"bloc_example/middlewares"
	"bloc_example/repository"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type router struct {
	Router *mux.Router
	repo   repository.Repository
}

func New(repo repository.Repository) (*router, error) {
	routerMux := mux.NewRouter().StrictSlash(true)
	return &router{
		Router: routerMux,
		repo:   repo,
	}, nil
}
func (r *router) StartService() {
	updateVideo, err := update.NewUpdateVideo(r.repo)
	if err != nil {
		panic(err)
	}
	updateComment, err := update.NewUpdateComment(r.repo)
	if err != nil {
		panic(err)
	}
	deleteVideo, err := delete.NewDeleteVideo(r.repo)
	if err != nil {
		panic(err)
	}
	deleteComment, err := delete.NewDeleteComment(r.repo)
	if err != nil {
		panic(err)
	}
	creates, err := create.New(r.repo)
	if err != nil {
		panic(err)
	}
	getRelatedVideos, err := get.NewGetRelatedVideos(r.repo)
	if err != nil {
		panic(err)
	}
	getVideos, err := get.NewGetVideos(r.repo)
	if err != nil {
		panic(err)
	}
	getComments, err := get.NewGetComments(r.repo)
	if err != nil {
		panic(err)
	}
	getUserVideos, err := get.NewGetUserVideos(r.repo)
	if err != nil {
		panic(err)
	}
	getVideo, err := get.NewGetVideo(r.repo)
	if err != nil {
		panic(err)
	}
	createVideo, err := create.NewVideo(r.repo)
	if err != nil {
		panic(err)
	}
	token, err := create.NewToken(r.repo)
	if err != nil {
		panic(err)
	}
	createUser, err := create.NewUser(r.repo)
	if err != nil {
		panic(err)
	}
	subRouter := r.Router.PathPrefix("").Subrouter()

	r.Router.HandleFunc("/", homeLink)
	r.Router.HandleFunc("/auth", token.GenerateToken).Methods("POST")
	r.Router.HandleFunc("/sign_up", createUser.RegisterUser).Methods("POST")
	r.Router.HandleFunc("/videos/{id}", updateVideo.UpdateVideo).Methods("PUT")
	subRouter.HandleFunc("/comments/{id}", updateComment.UpdateComments).Methods("PUT")
	subRouter.HandleFunc("/videos/{id}", deleteVideo.DeleteVideo).Methods("DELETE")
	subRouter.HandleFunc("/comments/{id}", deleteComment.DeleteComment).Methods("DELETE")
	subRouter.HandleFunc("/related_videos/{id}", getRelatedVideos.GetRelatedVideos).Methods("GET")
	subRouter.HandleFunc("/videos", createVideo.CreateVideo).Methods("POST")
	subRouter.HandleFunc("/videos", getVideos.GetAllVideos).Methods("GET")
	subRouter.HandleFunc("/videos/users/{user_id}", getUserVideos.GetUserVideos).Methods("GET")
	subRouter.HandleFunc("/videos/{id}/comments", getComments.GetComments).Methods("GET")
	subRouter.HandleFunc("/videos/{id}/comments", creates.CreateComments).Methods("POST")
	subRouter.HandleFunc("/videos/{id}", getVideo.GetVideo).Methods("GET")
	subRouter.Use(middlewares.Auth)
}

func homeLink(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode("Welcome to golang API example!")
}
