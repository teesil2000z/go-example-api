module bloc_example

go 1.19

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.7
	golang.org/x/crypto v0.1.0
	gopkg.in/paytm/grace.v1 v1.0.0-20150417064046-edb50d497280
)

require (
	github.com/stretchr/testify v1.8.0 // indirect
	gopkg.in/tylerb/graceful.v1 v1.2.15 // indirect
)
