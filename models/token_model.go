package model

type TokenModel struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Token string `json:"token"`
	Email string `json:"email"`
}

func NewToken(token string, name string, id int, email string) TokenModel {
	return TokenModel{
		Token: token,
		Name:  name,
		Id:    id,
		Email: email,
	}
}
