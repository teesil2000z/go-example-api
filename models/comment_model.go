package model

type Comment struct {
	Comment string `json:"comment"`
	Name    string `json:"name"`
	VideoId int    `json:"video_id"`
	UserId  int    `json:"user_id"`
}

type CommentResponse struct {
	Id      int    `json:"id"`
	Comment string `json:"comment"`
	Name    string `json:"name"`
	VideoId int    `json:"video_id"`
	UserId  int    `json:"user_id"`
}

type DeleteCommentParams struct {
	UserId int `json:"user_id"`
}
