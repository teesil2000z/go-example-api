package model

type Video struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Path        string `json:"path"`
	UserId      int    `json:"user_id"`
}

type VideoDTO struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Path        string `json:"path"`
	UserId      int    `json:"user_id"`
}

type DeleteVideoParams struct {
	UserId int `json:"user_id"`
}
