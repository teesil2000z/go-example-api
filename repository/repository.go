package repository

import (
	db "bloc_example/interfaces"
	"database/sql"
	"strconv"

	model "bloc_example/models"
)

type Repository struct {
	PostgresqlClient *sql.DB
}

func New() (*Repository, error) {
	postgresqlClient, err := db.GetInstance()
	if err != nil {
		return nil, err
	}

	return &Repository{
		PostgresqlClient: postgresqlClient,
	}, nil
}

func (r *Repository) CreateUserDB(name string, email string, password string) (bool, error) {
	_, err := r.PostgresqlClient.Exec("insert into users (name, email, password) VALUES ($1, $2, $3)", name, email, password)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *Repository) CheckUserCredentialsDB(email string) (*model.User, error) {
	rows, err := r.PostgresqlClient.Query("SELECT * FROM users WHERE email = $1", email)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	user := &model.User{}
	for rows.Next() {
		err := rows.Scan(&user.Id, &user.Email, &user.Password, &user.Name)
		if err != nil {
			return nil, err
		}
	}
	return user, nil
}

func (r *Repository) GetVideosFromRepository() ([]model.Video, error) {

	rows, err := r.PostgresqlClient.Query("select * from videos")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	video := []model.Video{}

	for rows.Next() {
		p := model.Video{}
		err := rows.Scan(&p.Id, &p.Name, &p.Description, &p.Path, &p.UserId)
		if err != nil {
			return nil, err
		}
		video = append(video, p)
	}
	if err != nil {
		return nil, err
	}

	return video, nil
}

func (r *Repository) GetUserVideosFromRepository(userId string) ([]model.Video, error) {

	rows, err := r.PostgresqlClient.Query("select * from videos where user_id = $1", userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	video := []model.Video{}

	for rows.Next() {
		p := model.Video{}
		err := rows.Scan(&p.Id, &p.Name, &p.Description, &p.Path, &p.UserId)
		if err != nil {
			return nil, err
		}
		video = append(video, p)
	}
	if err != nil {
		return nil, err
	}

	return video, nil
}

func (r *Repository) GetVideoCommentsFromRepository(id string) ([]model.CommentResponse, error) {
	rows, err := r.PostgresqlClient.Query("select * from comments where video_id = " + id)
	if err != nil {
		return nil, err
	}
	comments := []model.CommentResponse{}
	for rows.Next() {
		item := model.CommentResponse{}
		err := rows.Scan(&item.Id, &item.Name, &item.Comment, &item.VideoId, &item.UserId)
		if err != nil {
			return nil, err
		}
		comments = append(comments, item)
	}
	return comments, nil
}

func (r *Repository) GetVideoFromRepository(id string) (*model.Video, error) {
	rows, err := r.PostgresqlClient.Query("select * from videos where id = " + id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	p := &model.Video{}
	for rows.Next() {

		err := rows.Scan(&p.Id, &p.Name, &p.Description, &p.Path, &p.UserId)
		if err != nil {
			return nil, err
		}
	}
	return p, nil
}

func (r *Repository) CreateCommentInRepository(id string, comment model.Comment) (bool, error) {
	_, err := r.PostgresqlClient.Exec("insert into comments (name, comment, video_id, user_id) values ($1, $2, $3, $4)", comment.Name, comment.Comment, comment.VideoId, comment.UserId)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *Repository) UpdateCommentInRepository(id string, comment model.Comment) ([]model.CommentResponse, error) {
	_, err := r.PostgresqlClient.Exec("update comments set comment = '" + comment.Comment + "' where id = " + id)
	if err != nil {
		return nil, err
	}
	rows, err := r.PostgresqlClient.Query("select * from comments where video_id = " + strconv.Itoa(comment.VideoId))
	if err != nil {
		return nil, err
	}
	comments := []model.CommentResponse{}
	for rows.Next() {
		item := model.CommentResponse{}
		err := rows.Scan(&item.Id, &item.Name, &item.Comment, &item.VideoId, &item.UserId)
		if err != nil {
			return nil, err
		}
		comments = append(comments, item)
	}
	return comments, nil
}

func (r *Repository) UpdateVideoRepository(id string, video model.VideoDTO) ([]model.Video, error) {
	_, err := r.PostgresqlClient.Exec("update videos set name = '" + video.Name + "', description = '" + video.Description + "' where id = " + id)
	if err != nil {
		return nil, err
	}
	rows, err := r.PostgresqlClient.Query("select * from videos where user_id = " + strconv.Itoa(video.UserId))
	if err != nil {
		return nil, err
	}
	videos := []model.Video{}
	for rows.Next() {
		p := model.Video{}
		err := rows.Scan(&p.Id, &p.Name, &p.Description, &p.Path, &p.UserId)
		if err != nil {
			return nil, err
		}
		videos = append(videos, p)
	}
	return videos, nil
}

func (r *Repository) DeleteCommentInRepository(id string, comment model.DeleteCommentParams) (bool, error) {
	_, err := r.PostgresqlClient.Exec("Delete from comments where id = " + id + " AND user_id = " + strconv.Itoa(comment.UserId))
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *Repository) DeleteVideoInRepository(id string, comment model.DeleteVideoParams) (bool, error) {
	_, err := r.PostgresqlClient.Exec("Delete from videos where id = " + id + " AND user_id = " + strconv.Itoa(comment.UserId))
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *Repository) CreateVideoRepository(video model.VideoDTO) (bool, error) {

	_, err := r.PostgresqlClient.Exec("insert into videos (name, description, path, user_id) values ($1, $2, $3, $4)", video.Name, video.Description, video.Path, video.UserId)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *Repository) GetRelatedVideosRepository(id string, userId string) ([]model.Video, error) {
	rows, err := r.PostgresqlClient.Query("select * from videos where user_id = $1 and id != $2", userId, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	video := []model.Video{}

	for rows.Next() {
		p := model.Video{}
		err := rows.Scan(&p.Id, &p.Name, &p.Description, &p.Path, &p.UserId)
		if err != nil {
			return nil, err
		}
		video = append(video, p)
	}
	if err != nil {
		return nil, err
	}

	return video, nil
}
