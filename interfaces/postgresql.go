package postresql

import (
	"database/sql"

	_ "github.com/lib/pq"
)

const (
	dbname   = "api_glp3"
	host     = "dpg-cci7fpta4995109ju300-a.frankfurt-postgres.render.com"
	port     = "5432"
	user     = "api_glp3_user"
	password = "kBvrVtgmKSIQWNbphAYbKmoH6uiKwRkm"
	sslmode  = "require"
)

var instance *sql.DB

func GetInstance() (*sql.DB, error) {
	if instance == nil {
		connStr := "dbname=" + dbname + " host=" + host + " port=" + port + " user=" + user + " password=" + password + " sslmode=" + sslmode
		db, err := sql.Open("postgres", connStr)
		if err != nil {
			return nil, err
		}

		err = db.Ping()
		if err != nil {
			return nil, err
		}

		instance = db
	}
	return instance, nil
}
